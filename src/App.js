import React, { Component } from 'react';

import { BrowserRouter } from 'react-router-dom'
import Routes from './router';

import './styles/application.scss';

/*For Redux devtool*/
import { composeWithDevTools } from 'redux-devtools-extension';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import rootReducer from './reducers';
import createSagaMiddleware from 'redux-saga';
import {Saga} from './sagas';
const sagaMiddleware = createSagaMiddleware();

const store = createStore(rootReducer, composeWithDevTools(
  applyMiddleware(sagaMiddleware),
  // other store enhancers if any 
));

sagaMiddleware.run(Saga);

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <BrowserRouter>
          <div>
            <Routes />
          </div>
        </BrowserRouter>
      </Provider>
    );
  }
}
