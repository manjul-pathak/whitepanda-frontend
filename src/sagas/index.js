import { call, put, takeEvery, takeLatest } from 'redux-saga/effects';
import {loginWithEmail, signupWithEmail, loginWithPhone, otpVerify, emailVerify, sendForgotPassEmail, resetPassword, resendVerifyEmail } from '../lib/api';

function* _loginWithEmail(action) {
    try {
        const response = yield call(loginWithEmail, action.payload);
        yield put({ type: 'LOGIN_SUCCESSFUL', response: response })

    } catch (e) {
        yield put({ type: 'LOGIN_FAIL', response: e.response})
    }
}

function* _signupWithEmail(action) {
    try {
        const response = yield call(signupWithEmail, action.payload);
        yield put({ type: 'SIGNUP_WITH_EMAIL_SUCCESS', response: response })

    } catch (e) {
        yield put({ type: 'SIGNUP_FAIL', response: e.response })
    }
}

function* _loginWithPhone(action) {
    try {
        const response = yield call(loginWithPhone, action.payload);
        yield put({ type: 'OTP_SENT_SUCCESS', response: response })

    } catch (e) {
        yield put({ type: 'SIGNUP_FAIL', response: e.response })
    }
}

function* _otpVerify(action) {
    try {
        const response = yield call(otpVerify, action.payload);
        yield put({ type: 'OTP_VERIFY_SUCCESS', response: response })

    } catch (e) {
        yield put({ type: 'OTP_VERIFY_FAIL', response: e.response })
    }
}

function* _emailVerify(action) {
    try {
        const response = yield call(emailVerify, action.payload);
        yield put({ type: 'EMAIL_VERIFY_SUCCESS', response: response })

    } catch (e) {
        yield put({ type: 'EMAIL_VERIFY_FAIL', response: e.response })
    }
}

function* _sendForgotPassEmail(action) {
    try {
        const response = yield call(sendForgotPassEmail, action.email);
        yield put({ type: 'SEND_FORGOT_PASS_EMAIL_SUCCESS', response: response })

    } catch (e) {
        yield put({ type: 'SEND_FORGOT_PASS_EMAIL_FAIL', response: e.response })
    }
}

function* _resetPassword(action) {
    try {
        const response = yield call(resetPassword, action.payload);
        yield put({ type: 'RESET_PASSWORD_SUCCESS', response: response })

    } catch (e) {
        yield put({ type: 'RESET_PASSWORD_FAIL', response: e.response })
    }
}

function* _resendVerifyEmail(action) {
    try {
        console.log(action)
        const response = yield call(resendVerifyEmail, action);
        yield put({ type: 'RESEND_VERIFY_EMAIL_SUCCESS', response: response })

    } catch (e) {
        yield put({ type: 'RESEND_VERIFY_EMAIL_FAIL', response: e.response })
    }
}

export function* Saga() {
    yield takeEvery("LOGIN_WITH_EMAIL", _loginWithEmail);
    yield takeEvery("SIGNUP_WITH_EMAIL", _signupWithEmail);
    yield takeEvery("LOGIN_WITH_PHONE", _loginWithPhone);
    yield takeEvery("OTP_VERIFY", _otpVerify);
    yield takeEvery("EMAIL_VERIFY", _emailVerify);
    yield takeEvery("SEND_FORGOT_PASS_EMAIL", _sendForgotPassEmail);
    yield takeEvery("RESET_PASSWORD", _resetPassword);
    yield takeEvery("RESEND_VERIFY_EMAIL", _resendVerifyEmail);
}