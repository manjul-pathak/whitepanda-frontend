import React from 'react';
import { Switch, Route } from 'react-router-dom';

import LoginPage from '../containers/login';
import EmailVerifyPage from '../containers/verifyEmail';
import HomePage from '../containers/homePage';
import ResetPasswordPage from '../containers/resetPassword';

const Routes = () => (
  <div>
    <Switch>
    <Route exact path='/' component={HomePage}/>
      <Route exact path='/sso/login' component={LoginPage}/>
      <Route exact path='/verify-email' component={EmailVerifyPage}/>
      <Route exact path='/home' component={HomePage}/>
      <Route exact path='/reset-password' component={ResetPasswordPage}/>
      {/* <Route exact path='/signup' component={SignupPage}/>  
      <Route exact path='/' component={HomePage}/>
      <Route exact path='/verify/:q?' component={VerifyPage}/>
      <Route exact path='/forgot-password' component={ForgotPasswordPage}/>
      <Route exact path='/reset/:q?' component={ResetPasswordPage}/>
      <Route exact path='*' component={error}/> */}
    </Switch>
  </div>
)

export default Routes