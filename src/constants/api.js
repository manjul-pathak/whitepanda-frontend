import axios from 'axios';
import apiConstants from './apiConstants';

const initialStateSchema = {
  user: { token: '' }
};

let initialState = initialStateSchema;

const stateLocal = localStorage.getItem('user');
if (stateLocal && stateLocal !== undefined) {
  try {
    initialState = JSON.parse(stateLocal);
  } catch (e) {
    initialState = initialStateSchema;
  }
}

//axios.defaults.headers.common.Authorization = initialState.token;

const instance = axios.create({
  baseURL: apiConstants.API_URL,
  headers: {
    'Content-Type': 'application/json'
  }
});

export default instance;
