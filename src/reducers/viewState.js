const initialState = {
    currentComponent: ''
  };

export const changeViewState = (state = initialState, action) => {
    switch (action.type) {
        case 'CHANGE_VIEW_STATE':
            return {
                ...state,
                currentComponent: action.payload,
            }

        default:
            return state;
    }
}

