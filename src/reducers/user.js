import apiConstants from '../constants/apiConstants';
import { stat } from 'fs';

const initialStateSchema = {
  user: {},
  isFetching: false,
  error: [],
  verification: "",
  otpVerification: false,
  otpDelivery: false,
  resetPasswordResult: "INIT"
};

let initialState = initialStateSchema;

const stateLocal = localStorage.getItem('user');
if (stateLocal && stateLocal !== undefined) {
  try {
    initialState.user = JSON.parse(stateLocal);
  } catch (e) {
    initialState = initialStateSchema;
  }
}

export const userReducer = (state = initialState, action) => {
  let stateReturn = state;
  switch (action.type) {
    //On logging up with email
    case apiConstants.LOGIN_WITH_EMAIL:
      stateReturn = {
        ...state,
        isFetching: true
      };
      break;

    //On logging up with email
    case apiConstants.LOGIN_SUCCESSFUL:
      stateReturn = {
        ...state,
        isFetching: false,
        user: action.response.data
      };
      break;

    //On logging up with email
    case apiConstants.LOGIN_FAIL:
      stateReturn = {
        ...state,
        isFetching: false,
        error: [action.response.data]
      };
      break;

    //On signing up with email
    case apiConstants.SIGNUP_WITH_EMAIL:
      stateReturn = {
        ...state,
        isFetching: true
      };
      break;

    //On success
    case apiConstants.LOGIN_USER_SUCCESS:
      stateReturn = {
        ...state,
        isFetching: true
      };
      break;
    //On signup failure
    case apiConstants.SIGNUP_FAIL:
      stateReturn = {
        ...state,
        error: [action.response.data],
        isFetching: false
      };
      break;
    //On signup with email success
    case apiConstants.SIGNUP_WITH_EMAIL_SUCCESS:
      stateReturn = {
        ...state,
        user: action.response.data,
        isFetching: false
      };
      break;
    //On flushing errors
    case apiConstants.FLUSH_ERRORS:
      stateReturn = {
        ...state,
        error: []
      };
      break;
    //On flushing user state
    case apiConstants.FLUSH_USER_STATE:
      stateReturn = {
        ...state,
        user: {}
      };
      break;
    //On login using phone number
    case apiConstants.LOGIN_WITH_PHONE:
      stateReturn = {
        ...state,
        isFetching: true,
      }
      break;
    //On successfully sending the OTP
    case apiConstants.OTP_SENT_SUCCESS:
      stateReturn = {
        ...state,
        isFetching: false,
        user: action.response.data,
        otpDelivery: true
      }
      break;
    //On error while sending the OTP
    case apiConstants.OTP_SENT_FAIL:
      stateReturn = {
        ...state,
        isFetching: false,
        user: action.response,
        otpDelivery: false
      }
      break;
    //On verifying the OTP event
    case apiConstants.OTP_VERIFY:
      stateReturn = {
        ...state,
        isFetching: true
      }
      break;
    //On successfully verification of the OTP
    case apiConstants.OTP_VERIFY_SUCCESS:
      stateReturn = {
        ...state,
        isFetching: false,
        user: action.response.data,
        otpVerification: true
      }
      break;
    //On error while verifying the OTP
    case apiConstants.OTP_VERIFY_FAIL:
      stateReturn = {
        ...state,
        isFetching: false,
        error: [action.response.data]
      }
      break;

    //On successfully verifying email
    case apiConstants.EMAIL_VERIFY_SUCCESS:
      stateReturn = {
        ...state,
        verification: "true"
      }
      break;

    //On email verification fail
    case apiConstants.EMAIL_VERIFY_FAIL:
      stateReturn = {
        ...state,
        verification: "false"
      }
      break;

    //On email verification fail
    case apiConstants.SEND_FORGOT_EMAIL_PASS:
      stateReturn = {
        ...state,
        isFetching: true
      }
      break;

    //On email verification fail
    case apiConstants.SEND_FORGOT_EMAIL_PASS_SUCCESS:
      stateReturn = {
        ...state,
        isFetching: false
      }
      break;

    //On email verification fail
    case apiConstants.SEND_FORGOT_EMAIL_PASS_FAIL:
      stateReturn = {
        ...state,
        isFetching: false
      }
      break;

    //on reset password request 
    case 'RESET_PASSWORD':
      stateReturn = {
        ...state,
        isFetching: true
      }
      break;

    //on reset password request success
    case 'RESET_PASSWORD_SUCCESS':
      stateReturn = {
        ...state,
        isFetching: false,
        resetPasswordResult: "SUCCESS"
      }
      break;

    //on reset password request success
    case 'RESET_PASSWORD_FAIL':
      stateReturn = {
        ...state,
        isFetching: false,
        resetPasswordResult: "FAIL"
      }
      break;
    
    //on reset password request success
    case 'RESEND_VERIFY_EMAIL':
      stateReturn = {
        ...state,
        isFetching: false,
        resetPasswordResult: "FAIL"
      }
      break;
    
    //on reset password request success
    case 'RESEND_VERIFY_EMAIL_SUCCESS':
      stateReturn = {
        ...state,
        isFetching: false,
      }
      break;

    case apiConstants.LOGOUT_USER:
      stateReturn = { ...state, user: {} };
      break;
    case 'LOGIN_USER_ERROR':
      return {
        ...state,
        message: action.data.message
      };
    default:
      break;
  }

  if (stateReturn !== null) {
    if(action.type !== 'FLUSH_USER_STATE'){
      localStorage.setItem('user', JSON.stringify(stateReturn.user));
    }
    return stateReturn;
  }
};
