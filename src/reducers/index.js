import { combineReducers } from 'redux';
import {userReducer} from './user';
import {changeViewState} from './viewState';

export default combineReducers({
  user: userReducer,
  viewState: changeViewState
});
