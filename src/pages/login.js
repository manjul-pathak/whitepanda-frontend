import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import EmailLogin from '../components/emailLogin';
import OtpVerify from '../components/otpVerify';
import ForgotPassword from '../components/forgotPassword';
import ForgotPasswordConfirm from '../components/forgotPasswordConfirm';
import EmailSignup from '../components/emailSignup';
import EmailSignupConfirm from '../components/emailSignupConfirm';
import PhoneLogin from '../components/phoneLogin';

export default class Login extends Component {
	componentDidMount() {
		//this.props.pageType("error");		
	}

	render() {
		return (
			
			<div class="d-flex justify-content-center align-items-center login-container">
				
					{this.props.viewState.currentComponent == 'PHONE_LOGIN' && <PhoneLogin changeViewState={this.props.changeViewState} 
					loginWithPhone = {this.props.loginWithPhone}
					user={this.props.user}
					isFetching={this.props.isFetching}
					flushErrors={this.props.flushErrors} 
					flushUserState={this.props.flushUserState} 
					history={this.props.history}/>}

					{this.props.viewState.currentComponent == 'OTP_VERIFY' && <OtpVerify changeViewState={this.props.changeViewState} 
					otpVerify={this.props.otpVerify} 
					user={this.props.user} 
					isFetching={this.props.isFetching} 
					flushErrors={this.props.flushErrors} 
					flushUserState={this.props.flushUserState} 
					history={this.props.history} 
					otpVerification={this.props.otpVerification}/>}

					{this.props.viewState.currentComponent == 'FORGOT_PASSWORD' && <ForgotPassword changeViewState={this.props.changeViewState} 
					sendForgotPassEmail={this.props.sendForgotPassEmail} />}

					{this.props.viewState.currentComponent == 'FORGOT_PASSWORD_CONFIRM' && <ForgotPasswordConfirm changeViewState={this.props.changeViewState} />}
					
					{this.props.viewState.currentComponent == 'EMAIL_LOGIN' && <EmailLogin changeViewState={this.props.changeViewState} 
					loginWithEmail={this.props.loginWithEmail} 
					isFetching={this.props.isFetching} 
					flushErrors={this.props.flushErrors} 
					flushUserState={this.props.flushUserState} 
					history={this.props.history} />}
					
					{this.props.viewState.currentComponent == 'EMAIL_SIGNUP' && <EmailSignup changeViewState={this.props.changeViewState} 
					signupWithEmail = {this.props.signupWithEmail} 
					user={this.props.user} 
					isFetching={this.props.isFetching}
					flushErrors={this.props.flushErrors} 
					flushUserState={this.props.flushUserState} 
					history={this.props.history} />}
					
					{this.props.viewState.currentComponent == 'EMAIL_SIGNUP_CONFIRM' && <EmailSignupConfirm changeViewState={this.props.changeViewState} />}
					
					{this.props.viewState.currentComponent == '' && <EmailLogin changeViewState={this.props.changeViewState} 
					loginWithEmail={this.props.loginWithEmail} 
					user={this.props.user} 
					isFetching={this.props.isFetching} 
					flushErrors={this.props.flushErrors} 
					flushUserState={this.props.flushUserState} 
					history={this.props.history} />}

				</div>
		);
	}
}
