import React, { Component } from 'react';
import axios from 'axios';


export default class HomePage extends Component {
    componentDidMount() {
        console.log(this.props.history.location.state)
        if(!localStorage.getItem('token') && this.props.history.location.state !== 'register'){
            this.props.history.push('/sso/login');
        }
        this.checkLogin();
        //this.props.pageType("error");	
        //this.props.emailVerify(params.token);
    }

    checkLogin = () => {
        let config = {
            headers: {
              Authorization: localStorage.getItem('token'),
            }
          }
        axios.post(`https://api.awake.me/v1/awakeme/token`, {}, config).then(function(response){
            localStorage.setItem('token', response.data.data.token)
        }).catch(e => {
            this.handleLogout()
        });
    }

    handleLogout = () => {
        //delete items from localstorage
        localStorage.clear();
        this.props.flushUserState()
        this.props.history.push('/sso/login');
    }

    render() {
        if(localStorage.getItem('token')){
            if(localStorage.getItem('user')){
                var user = JSON.parse(localStorage.getItem('user'))
            }
            return (
                <div class="d-flex justify-content-center align-items-center login-container">
                    
                    <div className="login-form text-center">
                    <h1 class="mb-5 font-weight-light text-uppercase">Hi, {user.data.user.firstName}</h1>
                        <button class="btn mt-5 rounded-pill btn-lg btn-custom btn-block text-uppercase" id="send" onClick={this.handleLogout}>Logout</button>
                    </div>
                </div>
            );
        } else {
        return (
            <div class="d-flex justify-content-center align-items-center login-container">
                <img src="/Awake-Me-Logo.png"></img>
                <div className="login-form text-center">
                    <h1 class="mb-5 font-weight-light text-uppercase">We have sent an email to verify your account. Please check your inbox</h1>
                    <button class="btn mt-5 rounded-pill btn-lg btn-custom btn-block text-uppercase" id="send" onClick={this.handleLogout}>Got it</button>
                </div>
            </div>
        );
    }
    }
}
