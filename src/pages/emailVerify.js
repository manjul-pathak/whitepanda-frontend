import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import EmailLogin from '../components/emailLogin';
import OtpVerify from '../components/otpVerify';
import ForgotPassword from '../components/forgotPassword';
import ForgotPasswordConfirm from '../components/forgotPasswordConfirm';
import EmailSignup from '../components/emailSignup';
import EmailSignupConfirm from '../components/emailSignupConfirm';
import PhoneLogin from '../components/phoneLogin';
import queryString from 'query-string';
import Error from '../components/error';


export default class EmailVerifyPage extends Component {
	
	constructor(props) {
        super(props);
        this.state = {
            email: "",
			emailError: "",
			isEmailSent: false
        }
	}
	
	setEmail = (e) => {
        this.flushErrors();
        this.setState({
            email: e.target.value
        });
	}
	
	componentDidMount() {
        //this.props.pageType("error");	
		let params = queryString.parse(this.props.location.search)	
        this.props.emailVerify(params.token);
	}

	validateEmail = () => {
        let reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if (reg.test(this.state.email) == false) {
            this.setState({
                emailError: "Email is incorrect"
            });
            return false;
        } else {
            return true;
        }
    }

    handleResendEmail = () =>{
        if(this.validateEmail()){
			this.props.resendEmail(this.state.email);
			this.setState({
				isEmailSent: true
			})
           // this.handleClick("FORGOT_PASSWORD_CONFIRM");
        }
    }

    flushErrors = () => {
        this.setState({
            emailError: ""
        });
    }


    handleClick = (viewState) => {
        this.props.history.push('/');
    }

	render() {
		console.log(this.state)
		if(this.props.user.verification && this.props.user.verification === 'true'){
			return (
				<div class="d-flex justify-content-center align-items-center login-container">
					<div className="login-form text-center">
						<h1 class="mb-5 font-weight-light text-uppercase">Congratulations! Your email is verified</h1>
						<button class="btn mt-5 rounded-pill btn-lg btn-custom btn-block text-uppercase" id="send" onClick={() => this.handleClick()}>Go to Login</button>
					</div>
				</div>
			);
		}


		else if(this.state.isEmailSent === true){
			return (
				<div class="d-flex justify-content-center align-items-center login-container">
					<div className="login-form text-center">
						<h1 class="mb-5 font-weight-light text-uppercase">We have sent an email to verify your account. Please check your inbox</h1>						
                <button class="btn mt-5 rounded-pill btn-lg btn-custom btn-block text-uppercase" id="send" onClick={() => this.handleClick("EMAIL_LOGIN")}>Go back to Login</button>
					</div>
				</div>
			);
		} 

		
		else if(this.props.user.verification && this.props.user.verification === 'false'){
			return (
				<div class="d-flex justify-content-center align-items-center login-container">
					<div className="login-form text-center">
						<h1 class="mb-5 font-weight-light text-uppercase">Oops! We couldn't verify your email</h1>
						<div class="form-group">
                			<input className="form-control rounded-pill form-control-lg" type="text" id="Email" placeholder="Email" onChange={this.setEmail} />
                		</div>
						{this.state.emailError === "" ? "" : <Error msg={this.state.emailError} />}
						<button class="btn mt-5 rounded-pill btn-lg btn-custom btn-block text-uppercase" id="send" onClick={() => this.handleResendEmail()}>Resend Email </button>
                <span><strong>Or</strong></span>
                <button class="btn mt-5 rounded-pill btn-lg btn-custom btn-block text-uppercase" id="send" onClick={() => this.handleClick("EMAIL_LOGIN")}>Go back to Login</button>
					</div>
				</div>
			);
		}

		else{
			return (
				<div class="d-flex justify-content-center align-items-center login-container">
					<div className="login-form text-center">
						<h1 class="mb-5 font-weight-light text-uppercase">Verifying...</h1>
					</div>
				</div>
			);
		}

	}
}
