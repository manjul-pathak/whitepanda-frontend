import React, { Component } from 'react';
import queryString from 'query-string';
import Error from '../components/error';


export default class ResetPasswordPage extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            password: "",
            repeatPassword: "",
            passwordError: "",
            repeatPasswordError: "",
        }
    }
    
    componentDidMount() {
        //this.props.pageType("error");	
        //var params = queryString.parse(this.props.location.search)	
        //this.props.emailVerify(params.token);
    }

    setPassword = (e) => {
        this.flushErrors();
        this.setState({
            password: e.target.value
        });
    }

    setRepeatPassword = (e) => {
        this.flushErrors();
        this.setState({
            repeatPassword: e.target.value
        });
    }
    
    validatePassword = () => {
        if (this.state.password === "" || this.state.repeatPassword === "") {
            this.setState({
                passwordError: "Password can not be empty"
            }) 
            return false;
        } else if (this.state.password.length < 6) {
            this.setState({
                passwordError: "Password length must be greater than 6"
            })
            return false;
        } else if (this.state.password !== this.state.repeatPassword) {
            this.setState({
                repeatPasswordError: "Password and repeat password must be same"
            })
            return false;
        } else{
            return true;
        }
    }

    flushErrors = () => {
        this.setState({
            passwordError: "",
            repeatPasswordError: ""
        });
        //this.props.flushErrors();
    }

    handleClick = () => {
        if(this.validatePassword()){
            console.log(queryString.parse(this.props.location.search).token)
            this.props.resetPassword({password: this.state.password, token: queryString.parse(this.props.location.search).token});
        }
        //this.props.history.push('/');
    }

    goToLogin = () =>{ 
        this.props.history.push('/');
    }

	render() {
        if(this.props.user.resetPasswordResult == "SUCCESS"){
            return (
				<div class="d-flex justify-content-center align-items-center login-container">
					<div className="login-form text-center">
						<h1 class="mb-5 font-weight-light text-uppercase">Yay! Your password has been changed successfully</h1>
						<button class="btn mt-5 rounded-pill btn-lg btn-custom btn-block text-uppercase" id="send" onClick={() => this.goToLogin()}>Go to Login</button>
					</div>
				</div>
			);
        } else if(this.props.user.resetPasswordResult == "FAIL"){
			return (
				<div class="d-flex justify-content-center align-items-center login-container">
					<div className="login-form text-center">
						<h1 class="mb-5 font-weight-light text-uppercase">Oops! Your password could not be updated. </h1>
						<button class="btn mt-5 rounded-pill btn-lg btn-custom btn-block text-uppercase" id="send" onClick={() => this.goToLogin()}>Go to Login</button>
					</div>
				</div>
			);
        } else{
            return (
                <div class="d-flex justify-content-center align-items-center login-container">
                    <div className="login-form text-center">
                        <h1 class="mb-5 font-weight-light text-uppercase">Reset your password</h1>
                        <div className="form-group">
                        <input className="form-control rounded-pill form-control-lg" type="password" id="Password" onChange={this.setPassword} placeholder="Password" />
                        </div>
                        
                        {this.state.passwordError === "" ? "" : <Error msg={this.state.passwordError} />}
    
                        <div className="form-group">
                        <input className="form-control rounded-pill form-control-lg" type="password" id="Password" onChange={this.setRepeatPassword} placeholder="Repeat Password" />
                        </div>
                        {this.state.repeatPasswordError === "" ? "" : <Error msg={this.state.repeatPasswordError} />}
                        <button class="btn mt-5 rounded-pill btn-lg btn-custom btn-block text-uppercase" id="send" onClick={() => this.handleClick()}>{this.props.isFetching ? "Loading..": "Submit"}</button>
                    </div>
                </div>
            );
        }
	}
}
