import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Error from './error';

export default class ForgotPassword extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email: "",
            emailError: "",
        }
    }

    componentDidMount() {
        //this.props.pageType("error");
    }

    handleClick = (viewState) => {
        this.props.changeViewState(viewState);
    }

    setEmail = (e) => {
        this.flushErrors();
        this.setState({
            email: e.target.value
        });
    }

    validateEmail = () => {
        let reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if (reg.test(this.state.email) == false) {
            this.setState({
                emailError: "Email is incorrect"
            });
            return false;
        } else {
            return true;
        }
    }

    handleForgotPassword = () =>{
        if(this.validateEmail()){
            this.props.sendForgotPassEmail(this.state.email);
            this.handleClick("FORGOT_PASSWORD_CONFIRM");
        }
    }

    flushErrors = () => {
        this.setState({
            emailError: ""
        });
    }


    render() {
        return (
            <div className="login-form text-center">
                <img className="logo" src="/Awake-Me-Logo.png"></img>
                <h1 class="mb-5 font-weight-light text-uppercase">Forgot Password</h1>
                <div class="form-group">
                <input className="form-control rounded-pill form-control-lg" type="text" id="Email" placeholder="Email" onChange={this.setEmail} />
                </div>
                {this.state.emailError === "" ? "" : <Error msg={this.state.emailError} />}
                <button class="btn mt-5 rounded-pill btn-lg btn-custom btn-block text-uppercase" id="send" onClick={() => this.handleForgotPassword()}>Send Reset Instruction </button>
                <span><strong>Or</strong></span>
                <button class="btn mt-5 rounded-pill btn-lg btn-custom btn-block text-uppercase" id="send" onClick={() => this.handleClick("EMAIL_LOGIN")}>Go back to Login</button>
            </div>
        );
    }
}
