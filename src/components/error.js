import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class Error extends Component {

    componentDidMount() {
        //this.props.pageType("error");
    }

    render() {
        return (
            <div style={{'text-align':'left', margin:10 + 'px', 'color': 'red', 'font-size': 14 + 'px'}}>
                <label id="error">{this.props.msg}</label>
            </div>
        );
    }
}
