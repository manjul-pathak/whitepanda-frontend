import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import login from '../containers/login';
import { loginWithEmail } from '../lib/api';
import Error from './error';
import queryString from 'query-string';
import axios from 'axios';


export default class EmailLogin extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email: "",
            password: "",
            error: "",
            emailError: "",
            passwordError: ""
        }
        this.handleClick = this.handleClick.bind(this);
        this.handleLogin = this.handleLogin.bind(this);
        this.handleRedirect = this.handleRedirect.bind(this);
        this.props.flushUserState();
    }

    componentDidMount() {
        // if(localStorage.getItem('token')){
        //     let data = await axios.get("https://api.awake.me/v1/awakeme/get-redirect-url");
        //     window.location.href = data;
        // }
    }

    handleClick = (viewState) => {
        this.props.changeViewState(viewState);
    }

    setEmail = (e) => {
        this.props.flushErrors();
        this.setState({
            email: e.target.value,
            error: "",
            emailError: "",
            passwordError: ""
        });
    }

    setPassword = (e) => {
        this.props.flushErrors();
        this.setState({
            password: e.target.value,
            error: "",
            emailError: "",
            passwordError: ""
        });
    }

    handleRedirect = () => {
        let params = queryString.parse(this.props.history.location.search)
        axios.get(`https://api.awake.me/v1/awakeme/redirect-url/${params.appId}`).then(function(response){
        window.location.href = response.data.data.redirectUrl + `?token=${localStorage.getItem('token')}`;
        });
    }

    handleLogin = () => {
        if(this.validateEmail() && this.validatePassword()){
            if(this.props.history && this.props.history.location && this.props.history.location.search)
            {
                let params = queryString.parse(this.props.history.location.search)
                this.props.loginWithEmail({ email: this.state.email, password: this.state.password, appId: params.appId });
            } else{
                this.props.loginWithEmail({ email: this.state.email, password: this.state.password, provider: "EMAIL" });
            }
        }
    }

    validateEmail = () => {
        let reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if (reg.test(this.state.email) == false) {
            this.setState({
                emailError: "Email is incorrect"
            });
            return false;
        } else {
            return true;
        }
    }

    handleError = () => {
        this.props.user.error.map(item => {
            if(item.type === 'EMAIL_ERROR' && this.state.emailError == ''){
                this.setState({
                    emailError: item.message
                })
            } else if(this.state.emailError == ''){
                this.setState({
                    emailError: item.message
                });
            }
        })
    }

    handleClick = (viewState) => {
        this.props.changeViewState(viewState);
    }

    validatePassword = () => {
        if (this.state.password.length < 6) {
            this.setState({
                passwordError: "Password length must be greater than 6"
            })
            return false;
        } else{
            return true;
        }
    }

    render() {
        if(localStorage.getItem('token') && this.props.history && this.props.history.location && this.props.history.location.search){
            this.handleRedirect();
            return(
                <div></div>
            )
        }
        if(this.props.user && this.props.user.error.length > 0 && this.state.emailError === ''){
            this.handleError();
        }
        if(this.props.user && this.props.user.user && this.props.user.user.status === "success"){
            localStorage.setItem('token', this.props.user.user.data.token);
            if(this.props.user.user.data && this.props.user.user.data.redirectUrl)
            {
                window.location.href = this.props.user.user.data.redirectUrl;
            } else{
                this.props.history.push('/home');
            }
        }
        return (
            <div className="login-form text-center">
                <img className="logo" src="/Awake-Me-Logo.png"></img>
                <h1 class="mb-5 font-weight-light text-uppercase">Login</h1>
                <div className="form-group">
                    <input className="form-control rounded-pill form-control-lg" onChange={this.setEmail} type="text" id="Email" placeholder="Email" />
                </div>
                {this.state.emailError === "" ? "" : <Error msg={this.state.emailError} />}
                <div class="form-group">
                    <input className="form-control rounded-pill form-control-lg" onChange={this.setPassword} type="password" id="Password" placeholder="Password" />
                </div>
                {this.state.passwordError === "" ? "" : <Error msg={this.state.passwordError} />}
                {this.state.error === "" ? "" : <Error msg={this.state.error} />}
                <div class="forgot-link form-group d-flex justify-content-between align-items-center">
                    <a href="#"></a>
                    <span onClick={() => this.handleClick("FORGOT_PASSWORD")}  style={{ "cursor": "pointer"}}>Forgot your password?</span>
                </div>
                <button type="submit" class="btn mt-5 rounded-pill btn-lg btn-custom btn-block text-uppercase" onClick={() => this.handleLogin()}>{this.props.isFetching ? "Loading..": "Log in"}</button>
                <span onClick={() => this.handleClick("EMAIL_SIGNUP")}>Don't have an account? <strong style={{ "cursor": "pointer"}}>Register Now</strong></span>
                <br />
                {/* <span><strong>Or</strong></span>
                <button id="send" className="btn mt-5 rounded-pill btn-lg btn-custom btn-block text-uppercase" onClick={() => this.handleClick("PHONE_LOGIN")}>Continue with Phone Number</button> */}

            </div>
        );
    }
}
