import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Error from './error';
import queryString from 'query-string';

export default class EmailSignup extends Component {

    constructor(props) {
        super(props);
        this.state = {
            firstName: "",
            lastName: "",
            email: "",
            password: "",
            emailError: "",
            passwordError: "",
            repeatPasswordError: ""
        }
        this.props.flushUserState();
    }

    componentDidMount() {
        //this.props.pageType("error");
    }

    handleClick = (viewState) => {
        this.props.changeViewState(viewState);
    }

    setFirstName = (e) => {
        this.flushErrors();
        this.setState({
            firstName: e.target.value
        });
    }

    setLastName = (e) => {
        this.flushErrors();
        this.setState({
            lastName: e.target.value
        });
    }

    setEmail = (e) => {
        this.flushErrors();
        this.setState({
            email: e.target.value
        });
    }

    setPassword = (e) => {
        this.flushErrors();
        this.setState({
            password: e.target.value
        });
    }

    setRepeatPassword = (e) => {
        this.flushErrors();
        this.setState({
            repeatPassword: e.target.value
        });
    }

    handleSignup = () => {
        if(this.validateEmail() && this.validatePassword()){
            this.props.signupWithEmail({ firstName: this.state.firstName, lastName: this.state.lastName, email: this.state.email, password: this.state.password });
        }
    }


    validateEmail = () => {
        let reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if (reg.test(this.state.email) == false) {
            this.setState({
                emailError: "Email is incorrect"
            }) 
            return false;
        } else{
            return true;
        }
    }

    handleError = () => {
        this.props.user.error.map(item => {
            if(item.type === 'EMAIL_ERROR' && this.state.emailError === ''){
                this.setState({
                    emailError: item.message
                })
            } else if(this.state.emailError === ''){
                this.setState({
                    emailError: item.message
                });
            }
        })
    }

    validatePassword = () => {
        if (this.state.password === "" || this.state.repeatPassword === "") {
            this.setState({
                passwordError: "Password can not be empty"
            }) 
            return false;
        } else if (this.state.password.length < 6) {
            this.setState({
                passwordError: "Password length must be greater than 6"
            })
            return false;
        } else if (this.state.password !== this.state.repeatPassword) {
            this.setState({
                repeatPasswordError: "Password and repeat password must be same"
            })
            return false;
        } else{
            return true;
        }
    }

    flushErrors = () => {
        this.setState({
            emailError: "",
            passwordError: "",
            repeatPasswordError: ""
        });
        this.props.flushErrors();
    }

    render() {
        if(this.props.user && this.props.user.error.length > 0){
            this.handleError();
        }
        if(this.props.user && this.props.user.user && this.props.user.user.status === "success"){
            this.props.history.push('/', 'register');
        }
        return (
            <div className="login-form text-center">
                <img className="logo" src="/Awake-Me-Logo.png"></img>
                <h1 class="mb-5 font-weight-light text-uppercase">Sign Up</h1>

                <div className="form-group">
                <input className="form-control rounded-pill form-control-lg" type="text" id="FirstName" placeholder="First Name" onChange={this.setFirstName} />
                </div>
                <div className="form-group">
                <input className="form-control rounded-pill form-control-lg" type="text" id="LastName" placeholder="Last Name" onChange={this.setLastName} />
                </div>
                <div className="form-group">
                <input className="form-control rounded-pill form-control-lg" type="text" id="Email" placeholder="Email" onChange={this.setEmail} />
                </div>
                {this.state.emailError === "" ? "" : <Error msg={this.state.emailError} />}

                <div className="form-group">
                <input className="form-control rounded-pill form-control-lg" type="password" id="Password" onChange={this.setPassword} placeholder="Password" />
                </div>
                
                {this.state.passwordError === "" ? "" : <Error msg={this.state.passwordError} />}

                <div className="form-group">
                <input className="form-control rounded-pill form-control-lg" type="password" id="Password" onChange={this.setRepeatPassword} placeholder="Repeat Password" />
                </div>
                {this.state.repeatPasswordError === "" ? "" : <Error msg={this.state.repeatPasswordError} />}

                <button class="btn mt-5 rounded-pill btn-lg btn-custom btn-block text-uppercase" id="send" disabled={this.props.isFetching} onClick={this.handleSignup}>{this.props.isFetching ? "Loading..": "Register"}</button>
                {/* <span><strong>Or</strong></span>
                <button class="btn mt-5 rounded-pill btn-lg btn-custom btn-block text-uppercase" id="send" onClick={() => this.handleClick("PHONE_LOGIN")}>Continue with Phone Number</button> */}
                <span>Already on Awake? <strong onClick={() => this.handleClick("EMAIL_LOGIN")} style={{ "cursor": "pointer"}}>Log In</strong></span>
            </div>
        );
    }
}
