import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Error from './error';

export default class PhoneLogin extends Component {

    constructor(props) {
        super(props);
        this.state = {
            phone: "",
            phoneError: ""
        }
    }

    componentDidMount() {
        //this.props.pageType("error");
        this.props.flushUserState();
    }

    handleClick = () => {
        this.props.changeViewState("EMAIL_LOGIN");
    }

    setPhone = (e) => {
        this.flushErrors();
        const re = /^[0-9\b]+$/;
        // if value is not blank, then test the regex
        if ((e.target.value === '' || re.test(e.target.value))) {
           this.setState({phone: e.target.value})
        }
    }

    handlePhoneLogin = () => {
        //this.flushErrors();
        if(this.validatePhone()){
            this.props.loginWithPhone({ phone: this.state.phone, provider: "PHONE" });
        }
    }


    validatePhone = () => {
        if (this.state.phone.length < 10) {
            this.setState({
                phoneError: "Enter correct phone number"
            })
            return false;
        } else{
            return true;
        }
    }

    handleError = () => {
        this.props.user.error.map(item => {
            if(item.type === 'PHONE_ERROR' && this.state.phoneError === ''){
                this.setState({
                    phoneError: item.error
                })
            } else{
                this.setState({
                    phoneError: "Some error occured"
                });
            }
        })
    }

    flushErrors = () => {
        this.setState({
            phoneError: ""
        });
        this.props.flushErrors();
    }

    render() {
        if(this.props.user && this.props.user.error.length > 0){
            this.handleError();
        }
        if(this.props.user && this.props.user.user && this.props.user.user.status === "success"){
            this.props.changeViewState("OTP_VERIFY");
            //this.props.flushUserState();
        }
        
        return (
            <div className="login-form text-center">
                <h1 class="mb-5 font-weight-light text-uppercase">Continue with phone</h1>

                <div className="form-group">
                <input className="form-control rounded-pill form-control-lg" type="text" id="Email" maxlength="10" placeholder="Phone" onChange={this.setPhone} value={this.state.phone} />
                </div>
                {this.state.phoneError === "" ? "" : <Error msg={this.state.phoneError} />}

                <button class="btn mt-5 rounded-pill btn-lg btn-custom btn-block text-uppercase" id="send" disabled={this.props.isFetching} onClick={this.handlePhoneLogin}>{this.props.isFetching ? "Loading..": "Send OTP"}</button>
                <span><strong>Or</strong></span>

                <button class="btn mt-5 rounded-pill btn-lg btn-custom btn-block text-uppercase" id="send" onClick={() => this.handleClick("EMAIL_LOGIN")}>Continue with Email</button>
            
            </div>
        );
    }
}
