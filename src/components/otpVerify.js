import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Error from './error';

export default class OtpVerify extends Component {

    constructor(props) {
        super(props);
        this.state = {
            otp: "",
            otpError: ""
        }
    }

    setOtp = (e) => {
        this.flushError();
        const re = /^[0-9\b]+$/;
        // if value is not blank, then test the regex
        if ((e.target.value === '' || re.test(e.target.value))) {
            this.setState({ otp: e.target.value })
        }
    }

    componentDidMount() {
        //this.props.pageType("error");
        //this.props.flushUserState();
    }

    handleOtpVerify = () => {
        //this.flushErrors();
        if (this.handleValidation()) {
            this.props.otpVerify({ otp: this.state.otp, details: this.props.user.user.data.details });
            //this.props.flushUserState();
        }
    }

    handleValidation = () => {
        if (this.state.otp === "") {
            this.setState({
                otpError: "OTP can not be empty"
            })
            return false;
        } else if (this.state.otp.length < 6) {
            this.setState({
                otpError: "OTP length must be 6"
            })
            return false;
        } else {
            return true;
        }
    }

    handleError = () => {
        this.props.user.error.map(item => {
            if (item.type === 'OTP_ERROR' && this.state.otpError === '') {
                this.setState({
                    otpError: item.error
                })
            }
        })
    }

    flushError = () => {
        this.setState({
            otpError: ""
        });
        this.props.flushErrors();
    }

    render() {
        if (this.props.user && this.props.user.error.length > 0) {
            this.handleError();
        }
        if (this.props.user.otpVerification === true) {
            this.props.history.push('/home');
        }
        return (
            <div className="login-form text-center">

                <h1 class="mb-5 font-weight-light text-uppercase">Enter your OTP</h1>
                <div class="form-group">
                    <input className="form-control rounded-pill form-control-lg" type="text" id="Email" maxlength="6" placeholder="OTP" onChange={this.setOtp} value={this.state.otp} />
                </div>
                {this.state.otpError === "" ? "" : <Error msg={this.state.otpError} />}
                <button className="btn mt-5 rounded-pill btn-lg btn-custom btn-block text-uppercase" id="send" onClick={() => this.handleOtpVerify()}>{this.props.isFetching ? "Loading.." : "Verify OTP"}</button>
            </div>
        );
    }
}


