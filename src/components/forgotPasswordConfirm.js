import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class ForgotPasswordConfirm extends Component {

    componentDidMount() {
        //this.props.pageType("error");
    }

    handleClick = (viewState) => {
        this.props.changeViewState("EMAIL_LOGIN");
    }

    render() {
        return (
            <div class="d-flex justify-content-center align-items-center login-container">
            <div className="login-form text-center">
            <img className="logo" src="/Awake-Me-Logo.png"></img>
                <h1 class="mb-5 font-weight-light text-uppercase">We have sent you an email with intructions</h1>
                <button class="btn mt-5 rounded-pill btn-lg btn-custom btn-block text-uppercase" id="send" onClick={() => this.handleClick()}>Go to Login</button>
            </div>
        </div>
        );
    }
}
