import API from '../constants/api';
import axios from 'axios';
import apiConstants from '../constants/apiConstants';


export async function loginWithEmail(user) {
  const response = await API.post('/login', user).then(res => res);
  return response;
}

export async function signupWithEmail(user) {
  const response = await API.post('/register', user).then(res => res);
  return response;
}

export async function loginWithPhone(user) {
  const response = await API.post('/login', user).then(res => res);
  return response;
}

export async function otpVerify(payload) {
  const response = await API.post('/otp-verify', payload).then(res => res);
  return response;
}

export async function resendVerifyEmail(payload) {
  console.log(payload)
  const response = await API.post(`/resend-email?email=${payload.email}`, {}).then(res => res);
  return response;
}

export async function resetPassword(payload) {
  let config = {
    headers: {
      Authorization: payload.token,
    }
  }
  
  const response = await axios.post(`${apiConstants.API_URL}reset-password`, payload, config).then(res => res);
  return response;
}

export async function emailVerify(payload) {
  console.log(payload)
  let config = {
    headers: {
      Authorization: payload,
    }
  }
  const response = await axios.post(`${apiConstants.API_URL}validate-email`, {}, config).then(res => res);
  return response;
}

export async function sendForgotPassEmail(email) {
  const response = await API.get('/forgot-password?email=' + email).then(res => res);
  return response;
}

