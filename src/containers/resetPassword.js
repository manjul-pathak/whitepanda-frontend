import { connect } from 'react-redux'
import { emailVerify, changeViewState, resetPassword } from '../actions/auth'
import resetPasswordPage from '../pages/resetPassword'
 
const mapStateToProps = state => ({ 
  viewState: state.viewState,
  user: state.user,
  isFetching: state.user.isFetching,
  resetPasswordResult: state.user.resetPasswordResult
})
 
const mapDispatchToProps = dispatch => ({
    resetPassword: (payload) => dispatch(resetPassword(payload)),  
    changeViewState: (viewState) => dispatch(changeViewState(viewState)),
})
 
export default connect(
  mapStateToProps, 
  mapDispatchToProps
)(resetPasswordPage) 