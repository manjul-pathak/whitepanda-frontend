import { connect } from 'react-redux'
import { emailVerify, changeViewState, resendEmail } from '../actions/auth'
import emailVerifyPage from '../pages/emailVerify'
 
const mapStateToProps = state => ({ 
  viewState: state.viewState,
  user: state.user,
  isFetching: state.user.isFetching
})
 
const mapDispatchToProps = dispatch => ({
    emailVerify: (payload) => dispatch(emailVerify(payload)),
    resendEmail: (payload) => dispatch(resendEmail(payload)),  
    changeViewState: (viewState) => dispatch(changeViewState(viewState)),
})
 
export default connect(
  mapStateToProps, 
  mapDispatchToProps
)(emailVerifyPage) 