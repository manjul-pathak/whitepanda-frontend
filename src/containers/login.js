import { connect } from 'react-redux'
import { changeViewState, loginWithEmail, signupWithEmail, flushErrors, loginWithPhone, flushUserState, otpVerify, sendForgotPassEmail, resendEmail } from '../actions/auth'
import Login from '../pages/login'
 
const mapStateToProps = state => ({ 
  viewState: state.viewState,
  user: state.user,
  isFetching: state.user.isFetching, 
  otpVerification: state.otpVerification
})
 
const mapDispatchToProps = dispatch => ({
    changeViewState: (viewState) => dispatch(changeViewState(viewState)),
    loginWithEmail: (user) => dispatch(loginWithEmail(user)),
    signupWithEmail: (user) => dispatch(signupWithEmail(user)),
    flushErrors: () => dispatch(flushErrors()),
    flushUserState: () => dispatch(flushUserState()),
    loginWithPhone: (user) => dispatch(loginWithPhone(user)),
    otpVerify: (payload) => dispatch(otpVerify(payload)),  
    sendForgotPassEmail: (email) => dispatch(sendForgotPassEmail(email)),
    resendEmail: (email) => dispatch(resendEmail(email))
})
 
export default connect(
  mapStateToProps, 
  mapDispatchToProps
)(Login) 