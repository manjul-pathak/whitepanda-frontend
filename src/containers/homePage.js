import { connect } from 'react-redux'
import homePage from '../pages/homePage'
import {flushUserState} from '../actions/auth'
 
const mapStateToProps = state => ({ 
  viewState: state.viewState,
  user: state.user
})
 
const mapDispatchToProps = dispatch => ({
  flushUserState: () => dispatch(flushUserState()),
})
 
export default connect(
  mapStateToProps, 
  mapDispatchToProps
)(homePage) 