import apiConstants from '../constants/apiConstants';

export function setUserData(data) {
  return {
    type: apiConstants.LOGIN_USER_SUCCESS,
    data
  };
}

export function logout() {
  return {
    type: apiConstants.LOGOUT_USER
  };
}

export function signup(user) {
  return {
    type: "CHANGE_UI_STATE",
    user
  };
}


export const changeViewState = viewState =>({
	type:'CHANGE_VIEW_STATE',
	payload: viewState
})

export const loginWithEmail = user =>({
	type:'LOGIN_WITH_EMAIL',
	payload: user
})

export const signupWithEmail = user =>({
	type:'SIGNUP_WITH_EMAIL',
	payload: user
})

export const loginWithPhone = user =>({
	type:'LOGIN_WITH_PHONE',
	payload: user
})

export const emailVerify = token =>({
	type:'EMAIL_VERIFY',
	payload: token
})

export const otpVerify = payload =>({
	type:'OTP_VERIFY',
	payload
})

export const sendForgotPassEmail = email =>({
	type:'SEND_FORGOT_PASS_EMAIL',
	email
})

export const resendEmail = email =>({
	type:'RESEND_VERIFY_EMAIL',
	email
})

export const resetPassword = payload =>({
	type:'RESET_PASSWORD',
	payload
})

export const flushErrors = () =>({
	type:'FLUSH_ERRORS'
})

export const flushUserState = () =>({
	type:'FLUSH_USER_STATE'
})
